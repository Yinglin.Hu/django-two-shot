# Generated by Django 4.1.7 on 2023-03-03 23:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_receipt_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="number",
            field=models.CharField(max_length=20),
        ),
    ]
